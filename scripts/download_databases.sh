#!/usr/bin/env bash
if [ -z "${1}" ]; then
   echo "please provide a path for where to store the databases"
   read path
else
   path=$1
fi

if  [ -d "$path" ]; then
   git clone "http://gitlab-compare.cbs.dtu.dk/CGE/kmerfinder_db.git" $path/kmerfinder
   git clone "http://gitlab-compare.cbs.dtu.dk/CGE/mlst_db.git" $path/mlst
   git clone "http://gitlab-compare.cbs.dtu.dk/CGE/plasmidfinder_db.git" $path/plasmidfinder
   git clone "http://gitlab-compare.cbs.dtu.dk/CGE/pmlst_db.git" $path/pmlst
   #git clone "http://gitlab-compare.cbs.dtu.dk/CGE/prophages_db.git" $path/prophages
   git clone "http://gitlab-compare.cbs.dtu.dk/CGE/resfinder_db.git" $path/resfinder
   #git clone "http://gitlab-compare.cbs.dtu.dk/CGE/serotypefinder_db.git" $path/serotypefinder
   git clone "http://gitlab-compare.cbs.dtu.dk/CGE/virulencefinder_db.git" $path/virulencefinder
else
   echo "Error: Path '$path' does not exist!"
   exit
fi

# Always exit with ok status
exit 0
